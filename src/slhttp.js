class slHTTP {
    
    //Makes a HHTP GET Request
    async get(url) {
        const res = await fetch(url);   
        if(!res.ok) {
            await Promise.reject(new Error(res.status));
        }
        const data = await res.json();
        return data;
    }

    //Makes a HTTP POST Request
    async post(url,data) {
            const res = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(data)
            });
            if(!res.ok){
                await Promise.reject(Error(res.status));
            }
            const resData = await res.json();
            return resData;
        }
    //Makes a HTTP PUT Request
    
    async put(url,data) {
            const res = await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(data)
            });
                if(!res.ok){
                    await Promise.reject(Error(res.status));
                }
                const resData = await res.json();
                return resData;
            }
     //Makes a Delete Request
    async delete(url) {
            const res = await fetch(url, {
                method: 'DELETE'
            });
                if(!res.ok){
                    await Promise.reject(Error(res.status));
                }
                const data = await res.json();
            return ('Resource Deleted...');

    }
}

const http = new slHTTP();
export default http;

    
   

    
    