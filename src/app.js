import http from './slhttp';
import ui from './ui';

document.addEventListener('DOMContentLoaded', fetchPosts);

document.getElementById('add_post_btn').addEventListener('click', addPost);

document.getElementById('posts').addEventListener('click',deletePost);

document.getElementById('posts').addEventListener('click',editPost);
document.getElementById('edit_post_btn').addEventListener('click',updatePost);

//Get all posts
function fetchPosts() {
    http.get("http://localhost:3000/posts")
    .then(posts => ui.showPosts(posts))
    .catch(err => console.warn(err));
}

function addPost() {
    const title = document.getElementById('post_title').value;
    const body = document.getElementById('post_body').value;
    const author = document.getElementById('post_author').value;
    
    const data = {
        title,
        body,
        author
    };
    http.post("http://localhost:3000/posts", data)
    .then(post => {
        ui.showAlert('Post Created Successfully!','alert alert-success');
        ui.clearAllFields();
        fetchPosts();
    })
    .catch(err => console.warn(err));
}

function deletePost(e) {
    e.preventDefault();
    
    if(e.target.classList.contains('delete')) {
        const id = e.target.dataset.id;
        
        if(confirm('Are you sure you want to delete this post?')) {
            http.delete(`http://localhost:3000/posts/${id}`)
            .then(data => {
                ui.showAlert('Post deleted successfully', 'alert alert-info');
                fetchPosts();
            })
            .catch(err => console.warn(err));
        }
    }
    
    
}

function editPost(e){
    e.preventDefault();
    if(e.target.classList.contains('edit')){
        const id=e.target.dataset.id;
        
        //fetch the data of the post which you want to delete ie. having id as íd'
        http.get(`http://localhost:3000/posts/${id}`)
            .then(post => ui.fillModalData(post))
            .catch(err=>console.warn(err));
    }
}

function updatePost(e){
    const title = document.getElementById('edit_post_title').value;
    const body = document.getElementById('edit_post_body').value;
    const author = document.getElementById('edit_post_author').value;
    const editId = document.getElementById('edit_post_id').value;
    
    const data = {
        title,
        body,
        author
    };
    http.put(`http://localhost:3000/posts/${editId}`, data)
    .then(post => {
        ui.showAlert('Post Updated Successfully!','alert alert-info');
        fetchPosts();
    })
    .catch(err => console.warn(err));
    
    //close modal
    $('#editModal').modal('hide');
}


